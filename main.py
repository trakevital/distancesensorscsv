import tfmini_23 as tf
import Evo_Mini_py3 as Evo
import distance
import time
import csv
import math

if __name__ == "__main__":

	Evo_sensor = Evo.Evo_Mini()
	Evo_sensor.port.flushInput()
	Evo_sensor.set_binary_mode()  # Set binary output as it is required for this sample

	# Set ranging mode
	Evo_sensor.set_long_range_mode()

	Evo_sensor.set_single_pixel_mode()

	ranges = []
	

	res = open('results.csv', mode='w')
	try:
		res_writer = csv.writer(res,delimiter=',',quotechar='"')
		start_time = time.time()
		while ranges is not None:
			tf_distance, tf_strength = tf.getTFminiData()  
			Evo_result = Evo_sensor.get_ranges()[0]
			if math.isnan(Evo_result) or math.isinf(Evo_result):
				Evo_result = 0
			row_res = [str((time.time()-start_time)*1000),str(int(Evo_result*1000)),str(distance.get_distance()), str(tf_distance* 10), str(tf_strength)] # *10 to turn cm into mm  # *1000 to turn meter into milimeter
			print(row_res) #- debug
			res_writer.writerow(row_res) 
			time.sleep(0.01)
			
		else:
			print("No data from sensor")
	except KeyboardInterrupt:
		pass
	res.close()

		
